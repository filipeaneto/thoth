#ifndef THOTH_H
#define THOTH_H

#ifndef DEFAULT_THESAURUS_PATH
#define DEFAULT_THESAURUS_PATH "/usr/share/mythes/"
#endif // DEFAULT_THESAURUS_PATH

#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <list>
#include <map>

namespace Thoth
{

typedef std::string String;
typedef std::stringstream StringStream;
typedef std::list<String> StringList;

class Thesaurus
{
public:
    struct Entry
    {
        Entry();
        Entry(const String &definition, const StringList &meanings);
        Entry(const Entry &source);
        const Entry &operator=(const Entry &source);

        String definition;
        StringList meanings;
    };

    Thesaurus(const String &basename);
    Thesaurus(const String &indexPath, const String &dataPath);
    ~Thesaurus();

    Entry lookup(const String &text) const;

    static void SetDefaultPath(const String &path);
    static const String &GetDefaultPath();

private:
    typedef std::fstream FileStream;
    typedef std::size_t Index;
    typedef std::map<String, Index> IndexMap;
    typedef std::vector<Entry> EntryVector;

    void initialize(const String &indexPath, const String &dataPath);

    IndexMap mapping;
    EntryVector entries;

    String encoding;

    static String DefaultPath;
};

Thesaurus::Entry::Entry() { }

Thesaurus::Entry::Entry(const String &definition, const StringList &meanings) :
    definition(definition), meanings(meanings) { }

Thesaurus::Entry::Entry(const Entry &source) :
    definition(source.definition), meanings(source.meanings) { }

const Thesaurus::Entry &Thesaurus::Entry::operator=(const Entry &source)
{
    definition = source.definition;
    meanings = source.meanings;

    return *this;
}

Thesaurus::Thesaurus(const String &basename)
{
    String indexPath = DefaultPath;
    String dataPath = DefaultPath;

    indexPath.append("/th_");
    indexPath.append(basename);
    indexPath.append("_v2.idx");

    dataPath.append("/th_");
    dataPath.append(basename);
    dataPath.append("_v2.dat");

}

Thesaurus::Thesaurus(const String &indexPath, const String &dataPath)
{
    initialize(indexPath, dataPath);
}

Thesaurus::~Thesaurus()
{

}

Thesaurus::Entry Thesaurus::lookup(const String &text) const
{
    IndexMap::const_iterator index = mapping.find(text);

    if (index == mapping.end())
        return Entry(); // TODO exception

    return entries.at(index->second);
}

void Thesaurus::SetDefaultPath(const String &path)
{
    DefaultPath = path;
}

const String &Thesaurus::GetDefaultPath()
{
    return DefaultPath;
}

void Thesaurus::initialize(const String &indexPath, const String &dataPath)
{
    Index nEntries;
    StringStream ss;
    String line;

    FileStream indexFile(indexPath.c_str(), FileStream::in);
    FileStream dataFile(dataPath.c_str(), FileStream::in);

    std::getline(indexFile, encoding);
    std::getline(indexFile, line);
    ss << line;
    ss >> nEntries;

    entries.reserve(nEntries);

    for (Index i = 0; i < nEntries; i++)
    {
        StringStream ss;
        String word;
        Index index;

        unsigned pos = line.find_first_of("|");

        std::getline(indexFile, line);
        word = line.substr(0, pos - 1);
        ss << line.substr(pos + 1);
        ss >> index;

        mapping[word] = index;
    }

    // TODO ler o dataFile

    indexFile.close();
    dataFile.close();
}

String Thesaurus::DefaultPath = DEFAULT_THESAURUS_PATH;

}

#endif // THOTH_H
