#include "thoth.h"

#include <iostream>

using namespace Thoth;

int main(int argc, char *argv[])
{
    if (argc < 2)
    {
        std::cout << "usage: ./example language [default_path]\n";
        return 1;
    }

    if (argc > 2)
        Thesaurus::SetDefaultPath(argv[2]);

    std::cout << "Opening " << Thesaurus::GetDefaultPath() << '/' << argv[1] << ".{idx,dat}\n";
    Thesaurus db(argv[1]);

    String term;
    std::getline(std::cin, term);

    while (( std::cin.rdstate() & std::istream::eofbit ) == 0)
    {
        Thesaurus::Entry entry = db.lookup(term);

        std::cout << '\n';
        if (!entry.definition.empty())
        {
            std::cout << entry.definition << '\n';

            StringList::iterator it;
            for (it = entry.meanings.begin(); it != entry.meanings.end(); it++)
                std::cout << *it << '\n';
        }
        else
        {
            std::cout << '\"' << term << "\" not found.\n";
        }

        term.clear();
        std::getline(std::cin, term);
    }

    return 0;
}
